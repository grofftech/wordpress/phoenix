<?php
/**
 * Configuration
 *
 * @package     GroffTech\Phoenix\Config
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */
namespace GroffTech\Phoenix\Config;