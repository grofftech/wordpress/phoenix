<?php

/**
 * Theme Bootstrap
 *
 * @package     GroffTech\Phoenix
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */
namespace GroffTech\Phoenix;

use GroffTech\Phoenix\Asset\AssetVersioning;
use GroffTech\Phoenix\Settings\GenesisSettings;
use GroffTech\Phoenix\Structure as Structure;

require_once(__DIR__ . '/assets/vendor/autoload.php');

$theme = new PhoenixTheme(new AssetVersioning(), new GenesisSettings());
$menu = new Structure\Menu();