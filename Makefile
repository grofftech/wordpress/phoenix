# Executables
SASS=node_modules/.bin/node-sass
POST_CSS=node_modules/.bin/postcss
STYLELINT=node_modules/.bin/stylelint
WATCH=node_modules/.bin/watch
UGLIFY=node_modules/.bin/uglifyjs
ESLINT=node_modules/.bin/eslint
BROWSERSYNC=node_modules/.bin/browser-sync

# Configuration
CONFIG_DIR=config
STYLELINT_CONFIG=$(CONFIG_DIR)/stylelintrc.json
ESLINT_CONFIG=$(CONFIG_DIR)/.eslintrc.json
BROWSERSYNC_CONFIG=$(CONFIG_DIR)/browser-sync-config.js

# Sources
SOURCE_DIR=assets/src
CSS_SOURCE_DIR=$(SOURCE_DIR)/sass
JS_SOURCE_DIR=$(SOURCE_DIR)/js
SASS_FILE=$(SOURCE_DIR)/sass/style.scss
SASS_FILES=$(SOURCE_DIR)/sass/modules/*
JS_FILES=$(shell find $(JS_SOURCE_DIR) -type f)

# Distribution
DIST_DIR=assets/dist
BUILD_CSS_DIR=$(DIST_DIR)/css
BUILD_CSS_FILE=$(BUILD_CSS_DIR)/style.css
BUILD_CSS_FILE_MIN=$(BUILD_CSS_DIR)/style.min.css
BUILD_JS_DIR=$(DIST_DIR)/js
BUILD_JS_FILE=$(BUILD_JS_DIR)/app.js
BUILD_JS_FILE_MIN=$(BUILD_JS_DIR)/app.min.js
BUILD_IMAGES_DIR=$(DIST_DIR)/images
BUILD_LANGUAGES_DIR=$(DIST_DIR)/languages

all: clean $(BUILD_CSS_FILE) $(BUILD_JS_FILE)

$(BUILD_CSS_FILE): $(BUILD_CSS_DIR) sass-compile cssnano
	@ echo "Copying '$(BUILD_CSS_FILE)' and '$(BUILD_CSS_FILE_MIN)' to root directory..."
	@ cp $(BUILD_CSS_FILE) .
	@ cp $(BUILD_CSS_FILE_MIN) .

$(BUILD_CSS_DIR):
	@ echo "Creating directory '$(BUILD_CSS_DIR)'..."
	@ mkdir -p $@

sass-compile: stylelint
	@ echo "Compiling Sass..."
	@ $(SASS) $(SASS_FILE) \
		--output $(BUILD_CSS_DIR) \
		--output-style expanded \
		--source-map true \
		--quiet

stylelint:
	@ echo "Linting Sass..."
	@ $(STYLELINT) $(SASS_FILES) \
		--config $(STYLELINT_CONFIG)

cssnano: autoprefixer
	@ echo "Minifying CSS..."
	@ $(POST_CSS) \
		-u $@ \
		-o $(BUILD_CSS_FILE_MIN) \
		$(BUILD_CSS_FILE)

autoprefixer:
	@ echo "Autoprefixing CSS..."
	@ $(POST_CSS) \
		-u $@ \
		-r $(BUILD_CSS_FILE)

$(BUILD_JS_FILE): lint-js $(BUILD_JS_DIR)
	@ echo "Combining JS files..."
	@ cat $(JS_FILES) > $@
	@ echo "Minifying JS files..."
	@ cat $(JS_FILES) | $(UGLIFY) > $(BUILD_JS_FILE_MIN) \
		--compress \
		--mangle

$(BUILD_JS_DIR):
	@ echo "Creating directory '$(BUILD_JS_DIR)'..."
	@ mkdir -p $@

lint-js:
	@ echo "Linting JS..."
	@ $(ESLINT) \
		--config $(ESLINT_CONFIG) \
		$(JS_SOURCE_DIR)

browser-sync:
	@ $(BROWSERSYNC) start --config $(BROWSERSYNC_CONFIG)

clean:
	@ echo "Removing directory '$(DIST_DIR)'..."
	@ rm -rf $(DIST_DIR)

.PHONY: all clean sass-compile stylelint autoprefixer cssnano lint-js browser-sync
