# CHANGELOG

- Added make as build tool of choice
- Added dist folder to gitignore
- Moved various configuration files into their own config folder
- Moved assets into their own src folder
- Removed old npm scripts from package.json
- Updated asset loader to use minified Javascript file from assets/src/js folder
- Updated browsersync config file to account for custom placement of snippet when its injected into the DOM.
- Removed commented out code from functions.php
- Ignored responsive-menus.js for ES Lint