<?php
/**
 * Description
 *
 * @package     Namespace
 * @since       1.0.0
 * @author      Author
 * @link        Author Link
 * @license     GNU General Public License 2.0+
 */

namespace GroffTech\Phoenix\Tests\Unit;

if (version_compare(phpversion(), '5.6.0', '<')) {
    die("Phoenix unit tests requires PHP 5.6 or higher");
}

define('TEST_ROOT_DIR', __DIR__);
define('THEME_ROOT_DIR', dirname(dirname(dirname(__DIR__))) . DIRECTORY_SEPARATOR);
define('PROJECT_ROOT_DIR', dirname(dirname(dirname(dirname(__DIR__)))) . DIRECTORY_SEPARATOR );

$autoload_path = THEME_ROOT_DIR . 'assets/vendor/autoload.php';
if(!file_exists($autoload_path)) {
    die("Composer dependencies are required in order to run tests. Run 'composer install' and then try running the tests again.\n");
}

require_once(THEME_ROOT_DIR . 'assets/vendor/autoload.php');
unset($autoload_path);