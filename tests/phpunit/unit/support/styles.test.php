<?php
/**
 * Styles Test
 *
 * @package     GroffTech\Phoenix\Tests\Unit\Support
 * @since       1.0.0
 * @author      Author
 * @link        Author Link
 * @license     GNU General Public License 2.0+
 */

namespace GroffTech\Phoenix\Tests\Unit\Support;

use PHPUnit\Framework\TestCase;
use GroffTech\Phoenix\Support as Support;

class Test_Styles extends TestCase {

    protected function setUp() {
        parent::setUp();
        require_once THEME_ROOT_DIR . 'lib/support/styles.php';
    }

    /**
     * Test calculate_color_contrast should return white when color is brownish
     */
    public function test_should_return_white_for_color_contrast_when_brown_given() {
        $color_to_check = '#876546';
        $expected_contrast_color = '#ffffff';

        $actual_contrast_color = Support\calculate_color_contrast($color_to_check);

        $this->assertEquals($expected_contrast_color, $actual_contrast_color);
    }

    /**
     * Test calculate_color_contrast should return dark gray when color is whiteish
     */
    public function test_should_return_dark_gray_for_color_contrast_when_white_given()
    {
        $color_to_check = '#ffffff';
        $expected_contrast_color = '#333333';

        $actual_contrast_color = Support\calculate_color_contrast($color_to_check);

        $this->assertEquals($expected_contrast_color, $actual_contrast_color);
    }
}