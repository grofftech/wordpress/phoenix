<?php
/**
 * Description
 *
 * @package     Namespace
 * @since       1.0.0
 * @author      Author
 * @link        Author Link
 * @license     GNU General Public License 2.0+
 */

namespace GroffTech\Phoenix\Tests\Integration;

if (!file_exists('../../../wp-content')) {
    trigger_error('Unable to run the integration tests, as the wp-content folder does not exist.', E_USER_ERROR);
}

define('TEST_DIR', __DIR__);
define(
    'THEME_DIR',
    dirname(dirname(dirname(__DIR__))) . DIRECTORY_SEPARATOR
);
define(
    'WP_CONTENT_DIR',
    dirname(dirname(dirname(getcwd()))) . '/wp-content/'
);

if (!defined('PLUGIN_DIR')) {
    define('PLUGIN_DIR', WP_CONTENT_DIR . 'plugins/');
}

// Find and load the WordPress testing suite
function get_wp_tests_directory() {
    $tests_dir = getenv('WP_TESTS_DIR');

    if (empty($tests_dir)) {
        $tests_dir = '/var/opt/wordpress_core/tests/phpunit';
    }

    // If the tests' includes directory does not exist, try a relative path to Core tests directory.
    if (!file_exists($tests_dir . '/includes/')) {
        $tests_dir = '../../../../tests/phpunit';
    }

    // Check it again. If it doesn't exist, stop here and post a message as to why we stopped.
    if (!file_exists($tests_dir . '/includes/')) {
        trigger_error('Unable to run the integration tests, as the WordPress test suite could not be located.', E_USER_ERROR);
    }

    // Strip off the trailing directory separator, if it exists.
    return rtrim($tests_dir, DIRECTORY_SEPARATOR);
}

$tests_dir = get_wp_tests_directory();

# Get access to tests_add_filter() function.
require_once $tests_dir . '/includes/functions.php';

/**
 *
 */
tests_add_filter('setup_theme', function() {
    register_theme_directory(WP_CONTENT_DIR . 'themes');

    switch_theme(basename(THEME_DIR));
});

require $tests_dir . '/includes/bootstrap.php';

