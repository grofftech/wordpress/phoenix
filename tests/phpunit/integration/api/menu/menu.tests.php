<?php

namespace GroffTech\Phoenix\Tests\Integration\API\Menu;

use WP_UnitTestCase;

class MenuTests extends WP_UnitTestCase {

    public function test_should_move_nav_to_header() {
        $this->assertFalse(has_action('genesis_after_header', 'genesis_do_nav'));

        $this->assertEquals(12, has_action('genesis_header', 'genesis_do_nav'));
    }

    public function test_should_remove_nav_right() {
        $this->assertFalse(has_filter('wp_nav_menu_items', 'genesis_nav_right'));

        $this->assertFalse(has_filter('genesis_nav_items', 'genesis_nav_right'));
    }

    public function test_should_remove_sub_nav_after_header() {
        $this->assertFalse(has_action('genesis_after_header', 'genesis_do_subnav'));
    }
}