<?php
/**
 * Comments HTML Markup
 *
 * @package     GroffTech\Phoenix\Structure
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace GroffTech\Phoenix\Structure;

/**
 * Unregister comment callbacks
 *
 * @since 1.0.0
 *
 * @return void
 */
function unregister_comment_callbacks()
{

}

add_filter('genesis_comment_list_args', __NAMESPACE__ . '\setup_comments_gravatar');
/**
 * Modifies size of the Gravatar in the entry comments.
 *
 * @since 1.0.0
 *
 * @param array $args Gravatar settings.
 * @return array Gravatar settings with modified size.
 */
function setup_comments_gravatar(array $args)
{
    $args['avatar_size'] = 60;
    return $args;
}

