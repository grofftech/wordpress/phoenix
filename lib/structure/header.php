<?php

/**
 * Header HTML markup
 *
 * @package     GroffTech\Phoenix\Structure
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */
namespace GroffTech\Phoenix\Structure;

/**
 * Unregister header callbacks
 *
 * @since 1.0.0
 *
 * @return void
 */
function unregister_header_callbacks()
{

}