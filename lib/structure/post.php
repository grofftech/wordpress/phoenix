<?php

/**
 * Post(s) HTML markup
 *
 * @package     GroffTech\Phoenix\Structure
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */
namespace GroffTech\Phoenix\Structure;

/**
 * Unregister post callbacks
 *
 * @since 1.0.0
 *
 * @return void
 */
function unregister_post_callbacks()
{

}

add_filter('genesis_author_box_gravatar_size', __NAMESPACE__ . '\setup_author_box_gravatar');
/**
 * Modifies size of the Gravatar in the author box.
 *
 * @since 1.0.0
 *
 * @param int $size Original icon size.
 * @return int Modified icon size.
 */
function setup_author_box_gravatar(int $size)
{
    return 90;
}