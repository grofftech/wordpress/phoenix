<?php

/**
 * Menu(s) HTML markup
 *
 * @package     GroffTech\Phoenix\Structure
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */
namespace GroffTech\Phoenix\Structure;

class Menu {

    function __construct()
    {
        $this->unregister_menu_actions();
        add_action( 'genesis_header', 'genesis_do_nav', 12 );
        add_action( 'genesis_footer', 'genesis_do_subnav', 10 );

        $this->unregister_menu_filters();
        add_filter( 'wp_nav_menu_args', array( $this, 'setup_secondary_menu_args' ) );
    }

    public function unregister_menu_filters() {
        remove_filter('wp_nav_menu_items', 'genesis_nav_right', 10, 2);
        remove_filter('genesis_nav_items', 'genesis_nav_right', 10, 2);
    }

    public function unregister_menu_actions() {
        remove_action('genesis_after_header', 'genesis_do_nav');
        remove_action('genesis_after_header', 'genesis_do_subnav');
    }

    /**
     * Reduces secondary navigation menu to one level depth.
     *
     * @since 1.0.0
     *
     * @param array $args Original menu options.
     * @return array Menu options with depth set to 1.
     */
    public function setup_secondary_menu_args(array $args)
    {
        if ('secondary' !== $args['theme_location']) {
            return $args;
        }

        $args['depth'] = 1;
        return $args;
    }
}