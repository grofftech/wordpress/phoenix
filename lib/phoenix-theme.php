<?php

/**
 * Phoenix Theme
 *
 * @package     GroffTech\Phoenix
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace GroffTech\Phoenix;

class PhoenixTheme {

    private $versioning;
    private $genesisSettings;

    function __construct($versioning, $genesisSettings)
    {
        $this->versioning = $versioning;
        $this->genesisSettings = $genesisSettings;
        $this->init_constants();
        $this->toggle_url_redirection();
        add_action( 'wp_enqueue_scripts', array( $this, 'load_assets' ));
    }

    /**
     * Initialize the theme constants
     *
     * @since 1.0.0
     *
     */
    private function init_constants() {
        define('CHILD_THEME_DIR', get_stylesheet_directory());
        define('CHILD_THEME_URL', get_stylesheet_directory_uri());
        define('CHILD_CONFIG_DIR', CHILD_THEME_DIR . '/config/');

        $child_theme = wp_get_theme();

        define('CHILD_THEME_NAME', $child_theme->get('Name'));
        define('CHILD_THEME_VERSION', $this->versioning->get_theme_version());
        define('CHILD_TEXT_DOMAIN', $child_theme->get('TextDomain'));
    }

    /**
     * Toggles automatic URL redirection based on debug settings.
     *
     * If debug is off or not defined, redirect will be based on siteUrl settings
     * in the database, otherwise no redirection will happen.
     *
     * @since 1.0.0
     *
     * @return void
     */
    private function toggle_url_redirection() {
        if (!defined('WP_DEBUG') || constant('WP_DEBUG') === false) {
            return;
        }

        if (constant('WP_DEBUG')) {
            remove_filter('template_redirect', 'redirect_canonical');
        }
    }

    public function load_assets() {
        $this->load_styles();
        $this->load_scripts();
    }

    /**
     * Loads the styles.
     *
     * @since 1.0.0
     *
     * @return void
     */
    private function load_styles()
    {
        wp_enqueue_style(
            'genesis-sample-fonts',
            '//fonts.googleapis.com/css?family=Source+Sans+Pro:400,600,700',
            array(),
            $this->versioning->get_theme_version()
        );

        wp_enqueue_style(
            'dashicons'
        );

        wp_enqueue_style(
            'font-awesome',
            'https://use.fontawesome.com/releases/v5.0.8/css/all.css',
            array(),
            $this->versioning->get_theme_version()
        );
    }

    /**
     * Loads the scripts.
     *
     * @since 1.0.0
     *
     * @return void
     */
    function load_scripts()
    {
        $suffix = $this->versioning->developmentMode ? '' : '.min';
        $asset_file = "/assets/dist/js/app{$suffix}.js";

        wp_enqueue_script(
            'app',
            get_theme_file_uri( $asset_file ),
            array( 'jquery' ),
            $this->versioning->get_theme_version(),
            true
        );

        wp_localize_script(
            CHILD_TEXT_DOMAIN . '-responsive-menu',
            CHILD_TEXT_DOMAIN . '_responsive_menu',
            $this->genesisSettings->setup_responsive_menu_settings()
        );
    }
}