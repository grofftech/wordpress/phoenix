<?php
/**
 * Description
 *
 * @package     Namespace
 * @since       1.0.0
 * @author      Author
 * @link        Author Link
 * @license     GNU General Public License 2.0+
 */

namespace GroffTech\Phoenix\Settings;

class GenesisSettings {

    function __construct() {
        add_action( 'genesis_setup', array( $this, 'setup_child_theme' ), 15 );
        add_action( 'after_setup_theme', array( $this, 'setup_localization' ) );
        add_action( 'after_switch_theme', array ( $this, 'update_theme_setting_defaults' ) );

        add_filter( 'genesis_theme_settings_defaults', array( $this, 'set_theme_setting_defaults' ) );
    }

    /**
     * Setup the child theme.
     *
     * @since 1.0.0
     *
     * @return void
     */
    public function setup_child_theme()
    {
        $this->add_theme_supports();
        $this->add_image_sizes();
        $this->remove_site_layouts();
        $this->remove_widgets();
        $this->set_the_content_width();
    }

    /**
     * Sets localization (do not remove).
     *
     * @since 1.0.0
     */
    public function setup_localization()
    {
        load_child_theme_textdomain(CHILD_TEXT_DOMAIN, CHILD_THEME_DIR . '/assets/languages');
    }

    /**
     * Update the defaults for theme settings.
     *
     * @since 1.0.0
     *
     * @return void
     */
    public function update_theme_setting_defaults()
    {
        $config = $this->get_theme_setting_defaults();

        if (function_exists('genesis_update_settings')) {
            genesis_update_settings($config);
        }

        update_option('posts_per_page', $config['blog_cat_num']);
    }

    /**
     * Sets the defaults for theme settings.
     *
     * @since 1.0.0
     *
     * @param array $defaults
     * @return array A list of default theme settings
     */
    public function set_theme_setting_defaults($defaults)
    {
        $config = $this->get_theme_setting_defaults();
        $defaults = wp_parse_args($config, $defaults);

        return $defaults;
    }

    /**
    * Setup the responsive menu settings.
    *
    * @since 1.0.0
    *
    * @return array The configured settings
    */
    public function setup_responsive_menu_settings()
    {
        $settings = array(
            'mainMenu' => __('Menu', CHILD_TEXT_DOMAIN),
            'menuIconClass' => 'dashicons-before dashicons-menu',
            'subMenu' => __('Submenu', CHILD_TEXT_DOMAIN),
            'subMenuIconClass' => 'dashicons-before dashicons-arrow-down-alt2',
            'menuClasses' => array(
                'combine' => array(
                    '.nav-primary',
                ),
                'others' => array(),
            ),
        );

        return $settings;
    }

    /**
     * Adds theme supports.
     *
     * @since 1.0.0
     *
     * @return void
     */
    private function add_theme_supports()
    {
        $config = array(
            'html5' => array(
                'caption',
                'comment-form',
                'comment-list',
                'gallery',
                'search-form',
            ),
            'genesis-accessibility' => array(
                '404-page',
                'drop-down-menu',
                'headings',
                'rems',
                'search-form',
                'skip-links',
            ),
            'genesis-responsive-viewport' => '',
            'custom-logo' => array(
                'height' => 120,
                'width' => 700,
                'flex-height' => true,
                'flex-width' => true,
            ),
            'genesis-menus' => array(
                'primary' => __('Header Menu', CHILD_TEXT_DOMAIN),
                'secondary' => __('Footer Menu', CHILD_TEXT_DOMAIN),
            ),
            'genesis-after-entry-widget-area' => '',
            'genesis-footer-widgets' => 1
        );

        foreach ($config as $feature => $args) {
            add_theme_support($feature, $args);
        }
    }

    /**
    * Adds image sizes.
    *
    * @since 1.0.0
    *
    * @return void
    */
    private function add_image_sizes()
    {
        $config = array(
            'featured-image' => array(
                'width' => 720,
                'height' => 400,
                'crop' => true,
            ),
        );

        foreach ($config as $name => $args) {
            $crop = array_key_exists('crop', $args) ? $args['crop'] : false;
            add_image_size($name, $args['width'], $args['height'], $crop);
        }
    }

    /**
     * Removes site layouts.
     *
     * @since 1.0.0
     *
     * @return void
     */
    private function remove_site_layouts()
    {
        $config = array(
            'sidebar-content',
            'content-sidebar-sidebar',
            'content-sidebar-content',
            'sidebar-content-sidebar',
            'sidebar-sidebar-content',
        );

        foreach ($config as $layout) {
            genesis_unregister_layout($layout);
        }
    }

    /**
     * Removes any widgets or sidebars.
     *
     * @since 1.0.0
     *
     * @return void
     */
    private function remove_widgets()
    {
        $config = array(
            'header-right',
            'sidebar-alt',
        );

        foreach ($config as $widget) {
            unregister_sidebar($widget);
        }
    }

    /**
     * Get the theme setting defaults.
     *
     * @since 1.0.0
     *
     * @return array A list of default theme settings.
     */
    private function get_theme_setting_defaults()
    {
        return array(
            'blog_cat_num' => 12,
            'content_archive' => 'full',
            'content_archive_limit' => 0,
            'content_archive_thumbnail' => 0,
            'posts_nav' => 'numeric',
            'site_layout' => 'content-sidebar',
        );
    }

    /**
     * Sets the content width based on the theme's design and stylesheet.
     * https://codex.wordpress.org/Content_Width
     * https://wycks.wordpress.com/2013/02/14/why-the-content_width-wordpress-global-kinda-sucks/
     *
     * @since 1.0.0
     *
     * @return void
     */
    private function set_the_content_width()
    {
        $content_width = 702;
    }
}