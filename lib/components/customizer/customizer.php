<?php
/**
 * Customizer Handler
 *
 * @package     GroffTech\Phoenix\Components\Customizer
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */
namespace GroffTech\Phoenix\Components\Customizer;

use GroffTech\Phoenix\Support as Support;
use WP_Customize_Color_Control;

add_action( 'customize_register', __NAMESPACE__ . '\register_with_customizer');
/**
 * Registers settings and controls with the Customizer.
 *
 * @since 1.0.0
 *
 * @param WP_Customize_Manager $wp_customize Customizer object.
 */
function register_with_customizer($wp_customize) {
	$prefix = get_settings_prefix();

	$wp_customize->add_setting(
		$prefix . '_link_color',
		array(
			'default' => get_default_link_color(),
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			$prefix . '_link_color',
			array(
				'description' => __('Change the color of post info links, hover color of linked titles, hover color of menu items, and more.', CHILD_TEXT_DOMAIN),
				'label'       => __('Link Color', CHILD_TEXT_DOMAIN),
				'section'     => 'colors',
				'settings'    => $prefix . '_link_color',
			)
		)
	);

	$wp_customize->add_setting(
		$prefix . '_accent_color',
		array(
			'default' => get_default_accent_color(),
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			$prefix . '_accent_color',
			array(
				'description' => __('Change the default hovers color for button.', CHILD_TEXT_DOMAIN),
				'label'       => __('Accent Color', CHILD_TEXT_DOMAIN),
				'section'     => 'colors',
				'settings'    => $prefix . '_accent_color',
			)
		)
	);

	$wp_customize->add_setting(
		$prefix . '_logo_width',
		array(
			'default' => 350,
			'sanitize_callback' => 'absint',
		)
	);

	$wp_customize->add_control(
		$prefix . '_logo_width',
		array(
			'label' => __('Logo Width', CHILD_TEXT_DOMAIN),
			'description' => __('The maximum width of the logo in pixels.', CHILD_TEXT_DOMAIN),
			'priority'    => 9,
			'section'     => 'title_tagline',
			'settings'    => $prefix . '_logo_width',
			'type'        => 'number',
			'input_attrs' => array(
				'min' => 100,
			),
		)
	);
}
