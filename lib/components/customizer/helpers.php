<?php
/**
 * Helper functions for dealing with styles
 *
 * @package     GroffTech\Phoenix\Components\Customizer
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace GroffTech\Phoenix\Components\Customizer;

/**
* Get the settings prefix.
*
* @since 1.0.0
*
* @return void
*/
function get_settings_prefix() {
    return 'phoenix';
}
/**
 * Gets default link color for Customizer.
 *
 * @since 1.0.0
 *
 * @return string Hex color code for link color.
 */
function get_default_link_color()
{
    return '#0073e5';
}

/**
 * Gets default accent color for Customizer.
 *
 * @since 1.0.0
 *
 * @return string Hex color code for accent color.
 */
function get_default_accent_color()
{
    return '#0073e5';
}