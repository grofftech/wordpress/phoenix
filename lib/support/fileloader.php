<?php
/**
 * Loads files
 *
 * @package     GroffTech\Phoenix\Support
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */
namespace GroffTech\Phoenix\Support;

add_action('admin_init', __NAMESPACE__ . '\load_admin_files');
/**
* Loads files only required for admin purposes.
*
* @since 1.0.0
*
* @return void
*/
function load_admin_files() {
    $filenames = array (

    );

    load_specified_files($filenames);
}

/**
* Loads specified files.
*
* @since 1.0.0
*
* @param $filenames The filenames to load
* @param $directory The parent directory of the filenames

* @return void
*/
function load_specified_files($filenames, $directory = '') {
    $directory = $directory ?: CHILD_THEME_DIR . '/lib/';

    foreach($filenames as $files) {
        require_once($directory . $files);
    }
}

load_admin_files();


