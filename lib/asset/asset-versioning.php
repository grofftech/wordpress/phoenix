<?php
/**
 * Asset Versioning
 *
 * @package     GroffTech\Phoenix\Asset
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace GroffTech\Phoenix\Asset;

class AssetVersioning {
    public $developmentMode;

    function __construct()
    {
        add_filter( 'stylesheet_uri', array( $this, 'toggle_stylesheet'), 9999, 2 );
        $this->developmentMode = $this->is_site_in_development_mode();
    }

    /**
     * Gets the theme version.
     *
     * In development mode gets the current timestamp of the style.css file.
     * In non-development mode gets the version number at the top of the comment block of the style.css file.
     *
     * @since 1.0.0
     *
     * @return int|string The current timestamp\theme version.
     */
    public function get_theme_version()
    {
        $development_mode = $this->developmentMode;

        if ($development_mode) {
            return $this->get_asset_current_timestamp(get_stylesheet_directory() . '/style.css');
        }

        $theme = wp_get_theme();
        return $theme->get('Version');
    }

    /**
     * Get the current timestamp of an asset file.
     *
     * @since 1.0.0
     *
     * @return int The timestamp
     */
    private function get_asset_current_timestamp($asset_file)
    {
        return filemtime($asset_file);
    }

    /**
     * Checks to see if the site is in development mode.
     *
     * @since 1.0.0
     *
     * @return bool True if script debug config setting is set, otherwise false
     */
    private function is_site_in_development_mode()
    {
        if (!defined('SCRIPT_DEBUG')) {
            return false;
        }

        return constant('SCRIPT_DEBUG');
    }

    /**
     * Loads the non-minified or minified stylesheet.
     *
     * @since 1.0.0
     *
     * @param string $stylesheet_uri The uri for the current theme/child theme stylesheet.
     * @param string $stylesheet_dir_uri The uri for the current theme/child theme stylesheet directory.
     *
     * @return string The uri of the stylesheet
     */
    public function toggle_stylesheet($stylesheet_uri, $stylesheet_dir_uri)
    {
        $development_mode = $this->developmentMode;

        if ($development_mode) {
            return $stylesheet_uri;
        }

        return $stylesheet_dir_uri . '/style.min.css';
    }
}
