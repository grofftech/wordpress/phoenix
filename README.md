# Phoenix WordPress Child Theme

This is a developer-friendly theme for use as a starter child theme for WordPress projects using the Genesis Framework. It is based off of the Genesis Sample Theme offered by [Studio Press](https://www.studiopress.com/) as part of the [Genesis Framework](https://www.studiopress.com/features/).

This child theme is designed to be extended and customized to tailor for your project needs.

## Features

- Modular CSS using Sass
- [Autoprefixing](#autoprefixing)
- [Browsersync](#browsersync)
- [CSS Linting](#css-linting)
- [CSS Minification](#css-minification)
- [JavaScript Minification](#javascript-minification)
- [JavaScript Linting](#javascript-linting)
- Build with GNU Make

## Dependencies

- [Genesis Framework](https://my.studiopress.com/themes/genesis/) (requires purchase)
- [WordPress](https://wordpress.org/download/)
- [Node](https://nodejs.org/en/) (version 10.15.0 or higher)
- [Git](https://git-scm.com/)
- [GNU Make](https://www.gnu.org/software/make/)

## Installation

- Download the Genesis Framework from [Studio Press](https://www.studiopress.com/) and install into `wp-content\themes`.
- Open your WordPress project in a terminal client and navigate to `wp-content\themes`.
- Clone the repository with git, `git clone git@gitlab.com:grofftech/phoenix.git`
- Run `npm install`.
- Run `make all`.
- Log into the WordPress dashboard, navigate to `Appearance >> Themes` and activate `Phoenix`.
- Start customizing!

## Build

The build process for the theme is configured with GNU Make using a [Makefile](#makefile) in the theme's root folder. If you are using a Linux distribution, GNU Make is most likely installed by default. If you are using Windows, you'll have to use a tool such as [cygwin](https://www.cygwin.com/).

To run the build with [Browsersync](#browsersync), open a terminal and navigate to the theme's root folder and run:

```bash
make watch
```

To run the build without Browsersync, open a terminal and navigate to the theme's root folder and run:

```bash
make all
```

### Makefile

You are able to have your own custom config, source directory, and destination directories in the Makefile. All you need to do is update either the CONFIG, SOURCE_DIR or DIST_DIR variables with your custom directory names and all other variables will be taken care of.

### Autoprefixing

[Autoprefixer](https://github.com/postcss/autoprefixer) is being used for automatically adding vendor prefixes to CSS based on getting values from [Can I Use](http://caniuse.com/). It is configured to run as part of the main [build](#build) process for the theme.

It uses [Browserslist](https://github.com/browserslist/browserslist) for its configuration and is configurable through the "browserslist" configuration section in the `package.json` file located in the theme's root directory. By default it is configured with the default options below. The configuration can (and should) be adjusted to whatever suits your project's needs. See the [full list](https://github.com/browserslist/browserslist#full-list) for more options.

```json
"browserslist": [
    "> 0.5%",
    "last 2 versions",
    "Firefox ESR",
    "not dead"
  ]
```

### Browsersync

Browsersync will watch for changes in the CSS, HTML, JavaScript and PHP files. Any change in these files will automatically refresh the browser.

The Browsersync options are configured with `browser-sync-config.js` located in the `config` folder. See Browsersync's [documentation](https://browsersync.io/docs/options) for more information about the options available and what they do. Running Browsersync is part of the main [build](#build) for the theme.

### CSS Linting

This theme is configured to use [Stylelint](https://stylelint.io/) for Sass/CSS linting. It uses regular stylelint rules but also comes configured with the [stylelint-scss plugin](https://github.com/kristerkari/stylelint-scss), which adds some more specific rules for Sass.

A list of the complete stylelint rules is available [here](https://github.com/stylelint/stylelint/blob/3dc1972934270f3b45eb4ae6c417df7e01e54968/docs/user-guide/rules.md). The rules for SCSS plugin are available [here](https://github.com/kristerkari/stylelint-scss#list-of-rules).

The rules are configurable in the `stylelintrc.json` file in the `config` folder. There are only a few basic rules configured, feel free to add/edit/remove rules at your leisure. Any rules from the SCSS plugin will need to be prefixed with scss/.

Running stylelint is part of the main [build](#build) for the theme.

### CSS Minification

CSS is being minified using [CSS Nano](https://cssnano.co/) as a PostCSS plugin. Running CSS nano is part of the main [build](#build) for the theme.

### JavaScript Minification

JavaScript files are being concatenated together and then minified using the command line interface of [UglifyJS](https://github.com/mishoo/UglifyJS). If you need to configure any additional CLI parameters, you can do so for the task in the Makefile. Running uglify is part of the main part of the main [build](#build) for the theme.

### JavaScript Linting

JavaScript files are being linted using the command line interface of [ESLint](https://eslint.org/). The ESLint configuration is being handled by the `.eslinrc.json` file in the config folder. It is configured with a default set of rules and can be changed/updated as needed. See [https://eslint.org/docs/rules/](https://eslint.org/docs/rules/) for the list of rules. Running ESLint is  part of the main [build](#build) for the theme.
